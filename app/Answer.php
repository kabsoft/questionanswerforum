<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $guarded = [];

    public function comments() {
        return $this->hasMany(Comment::class);
    }
    public function answer_user_likes(){
        return $this->hasMany(answer_user_like::class);
    }

    public function question() {
        return $this->belongsTo(Question::class);
    }
    public function user() {
        return $this->belongsTo(User::class);
    }
}
