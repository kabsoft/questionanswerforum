<?php
namespace App\Datalayer\IRepositories;

use App\Question;


interface IAnswerRepo {
   public function GetAll();
   public  function AddNew($answer);
   public  function removeAnswer($answer);
   public  function UpdateAnswer($answer, $att);
}

?>
