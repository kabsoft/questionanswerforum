<?php
namespace App\Datalayer\IRepositories;

use App\Comment;

interface ICommentRepo {

   public function GetAll();
   public  function AddNew($comment);
   public  function removeComment($comment);
   public  function UpdateComment($comment, $att);

}

?>
