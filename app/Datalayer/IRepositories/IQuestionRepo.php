<?php
namespace App\Datalayer\IRepositories;

use App\Question;

interface IQuestionRepo {

   public function GetAll();
   public  function AddNew($question);
   public  function removeQuestion($question);
   public  function UpdateQuestion($question, $att);

}

?>
