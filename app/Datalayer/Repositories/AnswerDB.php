<?php

namespace App\Datalayer\Repositories;

use App\Datalayer\IRepositories\IAnswerRepo;
use Illuminate\Http\Request;
use App\Answer;





 class AnswerDB implements IAnswerRepo {
     public function GetAll(){
         return Answer::all();
     }
     public  function AddNew($answer){
         Answer::create($answer);
         return;
     }
     public  function UpdateAnswer($answer, $att){
         $answer->update($att);
         return;
     }
     public  function removeAnswer($answer){
         $answer->delete();
         return;
     }
 }

