<?php
namespace App\Datalayer\Repositories;

use Illuminate\Http\Request;
use App\Datalayer\IRepositories\IAnswerUserLikeRepo;
use App\answer_user_like;

class AnswerUserLikeDB implements IAnswerUserLikeRepo{

    public  function AddNew($like){

        answer_user_like::create($like);
        return;
    }
    public  function removeLike($like){
        $like->delete();
        return;
    }
}
