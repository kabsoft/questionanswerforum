<?php

namespace App\Datalayer\Repositories;

use App\Datalayer\IRepositories\ICommentRepo;
use Illuminate\Http\Request;
use App\Comment;





 class CommentDB implements ICommentRepo {
     public function GetAll(){
         return Comment::all();
     }

     public  function AddNew($comment){
         Comment::create($comment);
         return;
     }

     public  function UpdateComment($comment, $att){
         $comment->update($att);
         return;
     }

     public  function removeComment($comment){
         $comment->delete();
         return;
     }
 }

