<?php

namespace App\Datalayer\Repositories;

use Illuminate\Http\Request;
use App\Datalayer\IRepositories\IQuestionRepo;
use App\Question;




 class QuestionDB implements IQuestionRepo{
     public function GetAll(){
         return Question::all();
     }
     public  function AddNew($question){
         Question::create($question);
         return;
     }
     public  function UpdateQuestion($question, $att){
         $question->update($att);
         return;
     }
     public  function removeQuestion($question){
         $question->delete();
         return;
     }
 }

