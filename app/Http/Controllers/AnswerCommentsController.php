<?php

namespace App\Http\Controllers;

use App\Datalayer\Repositories\AnswerDB;
use App\Datalayer\Repositories\CommentDB;
use App\Answer;
use App\Comment;
use Illuminate\Http\Request;

class AnswerCommentsController extends Controller
{
    private $db;
    function __construct() {
        $this->db = new CommentDB();
        $this->middleware('verified');
    }

    public function edit( Comment $comment) {
        $this->authorize('update', $comment );
        return view('comments.edit', ['comment' => $comment]);
    }

    public function store() {
        $att = $this->validationPro();
        $att['user_id'] = auth()->id();
        $att['question_id'] = request('question_id');
        $att['answer_id'] = request('answer_id');
        $this->db->AddNew( $att );
        return back();
    }

    public function commentupdate(Comment $comment) {
        $this->authorize('update', $comment );

        $att = $this->validationPro();
        $this->db->UpdateComment($comment, $att );

        return redirect('/questions/' . $comment->question_id);
    }

    public function deleteComment(Comment $comment) {
        $this->authorize('delete', $comment );
        $this->destroy($comment);
        return back();
    }

    public function show(Comment $comment) {
        return view('comments.show', ['comment' => $comment]);
    }

    public function destroy(Comment $comment) {
        $this->authorize('delete', $comment );
        $this->db->removeComment($comment);
        return redirect('/questions/' . $comment->question_id);
    }

    public function validationPro() {
        return request()->validate([
            'description' => ['required', 'min:2']
        ]);
    }
}
