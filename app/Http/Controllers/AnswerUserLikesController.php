<?php

namespace App\Http\Controllers;

use App\Datalayer\Repositories\AnswerUserLikeDB;
use Illuminate\Http\Request;
use App\answer_user_like;

class AnswerUserLikesController extends Controller
{
    private $db;
    function __construct() {
        $this->db = new AnswerUserLikeDB();
        $this->middleware('verified');
    }

    public  function like(  ){

       $existingLike =  answer_user_like::where('user_id', request()->user_id)
                        ->where('question_id', request()->question_id)
                        ->where('answer_id', request()->answer_id)
                        ->first();

        if (is_null($existingLike)) {
            $this->db->AddNew( request(['user_id','question_id','answer_id'] ));
        }else{
            $this->destroy($existingLike);
        }
        $totalLike = answer_user_like::where('question_id', request()->question_id)
            ->where('answer_id', request()->answer_id)
            ->count();
        return $totalLike;
    }

    public function destroy(answer_user_like $like) {
        $this->db->removeLike($like);
        return;
    }
}
