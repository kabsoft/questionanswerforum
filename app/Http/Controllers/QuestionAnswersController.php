<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Datalayer\Repositories\QuestionDB;
use App\Datalayer\Repositories\AnswerDB;
use App\Answer;
use App\Question;

class QuestionAnswersController extends Controller
{
    private $db;
    function __construct() {
        $this->db = new AnswerDB();
        $this->middleware('verified');
    }

    public function edit( Answer $answer) {
        $this->authorize('update', $answer );
        return view('answers.edit', ['answer' => $answer]);
    }

    public function answerupdate(Answer $answer) {
        $this->authorize('update', $answer );
         $att = $this->validationPro();
         $this->db->UpdateAnswer($answer,$att);
        return redirect('/questions/' . $answer->question_id);
    }

    public function deleteAnswer(Answer $answer) {
        $this->authorize('delete', $answer );
        $this->destroy($answer);
        return back();
    }

    public function show(Answer $answer) {
        return view('answers.show', ['answer' => $answer]);
    }

    public function store() {
        $att = $this->validationPro();
        $att['user_id'] = auth()->id();
        $att['question_id'] = request('question_id');
        $this->db->AddNew( $att );
        return back();
    }

    public function destroy(Answer $answer) {
        $this->authorize('delete', $answer );
        $this->db->removeAnswer($answer);
        return redirect('/questions/' . $answer->question_id);
    }

    public function validationPro() {
        return request()->validate([
            'description' => ['required', 'min:3']
        ]);
    }
}
