<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Datalayer\Repositories\QuestionDB;
use App\Question;



class QuestionsController extends Controller
{
    private $db;
    function __construct() {
        $this->db = new QuestionDB();
        $this->middleware('verified');
    }

    public function index() {
        return view('questions.index', ['questions' => $this->db->GetAll()]);
    }

    public function show(Question $question) {
        return view('questions.show', ['question' => $question]);
    }

    public function edit(Question $question) {
        $this->authorize('update', $question );
        return view('questions.edit', ['question' => $question]);
    }

    public function create() {
        return view('questions.create');
    }

    public function store() {
        $att = $this->validationPro();
        $att['user_id'] = auth()->id();
        $this->db->AddNew($att);
        return redirect('/questions');
    }

    public function update(Question $question) {
        $this->authorize('update', $question );
        $att = $this->validationPro();
        $this->db->UpdateQuestion($question, $att);
        return redirect('/questions');
    }

    public function destroy(Question $question) {

        $this->authorize('delete', $question );
        $this->db->removeQuestion($question);
        return redirect('/questions');
    }

    public function validationPro() {
        return request()->validate([
            'title' => ['required', 'min:5'],
            'description' => ['required', 'min:10']
        ]);
    }

    //documentation
    public function documentation(){
        return view('documentation');
    }
}
