<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class answer_user_like extends Model
{
    protected $guarded = [];

    public function answer() {
        return $this->belongsTo(Answer::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
