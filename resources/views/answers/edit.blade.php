@extends('layoutBulma')

@section('content')
<h1 class="title"> Edit Answer </h1>
<div class="panel">
    <form method="Post" action="/answers/{{ $answer->id}}/answerupdate">
        @csrf
        @method('patch')

        <div class="panel">
            <label class="label" for="description"> Description </label>
            <div>
                <textarea class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name="description" >{{ $answer->description}}</textarea>
            </div>
        </div>
        <div>
            <button type="submit" class="button is-link">Update answer</button>
        </div>
    </form>
</div>

@include('errors')
@endsection

