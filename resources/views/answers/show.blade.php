@extends('layoutBulma')

@section('content')
<div class="panel">
    <div class="panel">
        <label class="label"> Description: </label>
        <div> {{ $answer->description }} </div>
    </div>
</div>


<div class="panel">
    <a href="/answers/{{$answer->id}}/edit" class="button is-link" role="button"> Edit answer </a>
</div>
<!-- Delete answer -->
<div class="panel">
    <form method="Post" action="/answers/{{$answer->id}}" id="deleteAnswerFrm">
        @csrf
        @method('delete')
        <a href="#" class="button is-danger deleteanswerbtn" role="button" onclick="deleteAnswer('{{$answer->description}}' )"> Delete answer </a>
    </form>
</div>
@endsection
