@extends('layoutBulma')

@section('content')
<h1 class="title"> Edit Comment </h1>
<div class="panel">
    <form method="Post" action="/comments/{{ $comment->id }}/commentupdate">
        @csrf
        @method('patch')

        <div class="panel">
            <label class="label" for="description"> Description </label>
            <div>
                <textarea class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name="description" >{{ $comment->description}}</textarea>
            </div>
        </div>
        <div>
            <button type="submit" class="button is-link">Update Comment</button>
        </div>
    </form>
</div>

@include('errors')
@endsection

