@extends('layoutBulma')

@section('content')



<div class="content">   
    <h1 class="title is-3"> Create a basic Q&A app with PHP. Similar concept as Stack Overflow. </h1>
    <p class="title is-5">Back end</p>
    <ul class="menu-list">
        <li><a>PHP 7.1.16 </a></li>
        <li><a>MySql: 5.7.21</a></li>
        <li><a>Laravel Framework: 5.8.10 </</li> 
        <li><a>Eloquent ORM </a></li>
        <li><a>RestAPI </a></li>
    </ul>

    <p class="title is-5">Front end</p>
    <ul class="menu-list">
        <li><a>Bulma : CSS Framework </a></li>
        <li><a>jQuery</a></li>
        <li><a>blade</a></li>
    </ul>

    <p class="title is-5">Tools</p>
    <ul class="menu-list">
        <li><a> Artisan CLI </a></li>
        <li><a>Composer</a></li>  
        <li><a>Git</a></li>
        <li><a>DB migration tool</a></li>
    </ul>

    <p class="title is-5"> Functionality: </p>
    <ul class="menu-list">
        <li>  User registrations with email confirmation and password reset functionality </li>
        <li>  Logged in users can:
            <ul>
                <li>See a list of all questions</li>
                <li>Post questions, comments and answers</li>
            </ul>
        </li>
        <li>  Like functionality </li>
        <li>  Property and method visibility ( Logged in user can read everything, but only owner can edit/update/delete ) </li>
        <li>  Simple REST API for interaction with the content
            <ul>
                <li> Question: CRUD functionality</li>
                <li>Answer:   CRUD functionality</li>
                <li>Comment:  CRUD functionality</li>
            </ul>
        </li>
        <li>  If a Question is deleted, corresponding answers and comments will be deleted </li>
        <li>  If an answer is deleted, corresponding comments will be deleted </li>
        <li> Validation Client side and server side(Comment validation has not done yet) </li>

    </ul>

    <p class="title is-5">How to run:</p>
     <ul class="menu-list">
        <li><a>Copy the project folder or clone from git and navigate terminal/cmd just run following commands. </a></li>
        <li><a>Create database and place the same name at .env file in laravel project folder </a></li>
        <li><a> CMD.. project folder> </a>
            <ol>
                <li><a> composer install </a></li>
                <li><a> php artisan key:generate   </a> </li>
                <li><a> php artisan cache:clear </a> </li>
                <li><a> php artisan migrate </a> </li>
                <li><a> php artisan serve </a> </li>
            </ol>

        </li>
        <li>  Browser: localhost:8000 </li>
    </ul>
    <div class="panel">
        <p class="title is-5">Files</p>
        <div class="panel">
            <div class="panel-block"> Controller: </div>
            <div class="panel-block">   app/Http/Controllers/QuestionsController.php </div>
            <div class="panel-block">  app/Http/Controllers/QuestionAnswersController.php </div>
            <div class="panel-block">  app/Http/Controllers/AnswerCommentsController.php </div>
            <div class="panel-block">  app/Http/Controllers/AnswersUserLikesController.php </div>

        </div>

        <div class="panel">
            <div class="panel-block"> Datalayer: </div>
            <div class="panel-block">  app/Datalayer/IRepositories (Interface) </div>
            <div class="panel-block"> app/Datalayer/Repositories   (implementation) </div>
        </div>

        <div class="panel">
            <div class="panel-block"> Model: </div>
            <div class="panel-block">  app/Question.php </div>
            <div class="panel-block">  app/Answer.php </div>
            <div class="panel-block">  app/Comment.php </div>
            <div class="panel-block">  app/Answer.php </div>
            <div class="panel-block">  app/User.php </div>
            <div class="panel-block">  app/answer_user_like.php </div>
        </div>

        <div class="panel">
            <div class="panel-block"> View: </div>
            <div class="panel-block">  resources/views/questions </div>
            <div class="panel-block">  resources/views/answers </div>
            <div class="panel-block">   resources/views/comments </div>
        </div>

         <div class="panel">
            <div class="panel-block"> Routes: </div>
            <div class="panel-block">   routes/web.php </div>           
        </div>
    </div>

</div>

@endsection
