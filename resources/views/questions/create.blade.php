@extends('layoutBulma')

@section('content')
<h1 class="title"> Create Question</h1>
<div>
    <form method="Post" action="/questions">
        @csrf
        <div class="panel">
            <label class="label" for="title"> Title (Max length: 255 char)</label>
            <div>
                <input type="text" name='title' class="input {{ $errors->has('title') ? 'is-danger': '' }}" value="{{ old('title') }}" required>
            </div>
        </div>
        <div class="panel">
            <label class="label" for="description"> Description </label>
            <div>
                <textarea class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name="description" >{{old('description')}}</textarea>
            </div>
        </div>
        <div>
            <button type="submit" class="button is-link">Create new Question</button>
        </div>
    </form>
</div>
@include('errors')
@endsection

