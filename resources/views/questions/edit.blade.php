@extends('layoutBulma')

@section('content')
<h1 class="title"> Edit question </h1>
<div class="panel">
    <form method="Post" action="/questions/{{ $question->id}}">
        @csrf
        @method('patch')
        <div class="panel">
            <label class="label" for="title"> Title </label>
            <div>
                <input type="text" name='title' class="input {{ $errors->has('title') ? 'is-danger': '' }}" value="{{ $question->title }}" required>
            </div>
        </div>
        <div class="panel">
            <label class="label" for="description"> Description </label>
            <div>
                <textarea class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name="description" >{{ $question->description}}</textarea>
            </div>
        </div>
        <div>
            <button type="submit" class="button is-link">Update question</button>
        </div>
    </form>
</div>
@include('errors')
@endsection

