@extends('layoutBulma')

@section('content')
    <!-- h1> Index page </h1 -->
    @if($questions->count())
        <div class="panel">
            <p class="panel-heading">Questions:</p>

                @foreach($questions->reverse() as $question)
                <div class="box">
                    <ul>

                    <!-- for username and date -------->
                    <li>
                        <div>
                            <ul>
                                <li><img src="/images/comment.jpg" width="30px" height="30px"> {{ $question->user->name}}</li>
                                <li>
                                    @if( $question->created_at != $question->updated_at )
                                        <div>
                                            <p> Updated: {{ $question->updated_at  }} </p>
                                        </div>
                                    @else
                                        <div>
                                            <p> Created: {{ $question->created_at  }} </p>
                                        </div>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </li>
                        <!-- End for username and date -------->

                    <li>
                        <a href="/questions/{{ $question->id }}"> {{ $question->title }} </a>
                    </li>
                    </ul>
                </div>
                @endforeach

        </div>
    @endif
@endsection
