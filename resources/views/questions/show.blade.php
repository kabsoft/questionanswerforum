@extends('layoutBulma')

@section('content')

@if($question->count())

<div class="box margintop20px">
<div class="panel">
    <!-- for username and date -------->
    <div>
        <ul>
            <li><img src="/images/comment.jpg" width="30px" height="30px"> {{ $question->user->name}}</li>
            <li>
                @if( $question->created_at != $question->updated_at )
                    <div>
                        <p> Updated: {{ $question->updated_at  }} </p>
                    </div>
                @else
                    <div>
                        <p> Created: {{ $question->created_at  }} </p>
                    </div>
                @endif
            </li>
        </ul>
    </div>
    <!-- End for username and date -------->
    <div class="panel">
        <label class="label">Question:</label>
        <p>{{ $question->title }}</p>
    </div>
    <div class="panel">
        <label class="label"> Description: </label>
        <div> {{ $question->description }} </div>
    </div>
</div>

<div class="panel columns">
    @can('update', $question)
    <div class="column is-one-fifth">
        <a href="/questions/{{ $question->id }}/edit" class="button is-link"> Edit Queestion </a>
    </div>
        @endcan
    @can('delete', $question)
        <div class="column is-one-fifth">
            <form method="Post" action="/questions/{{$question->id}}">
                @csrf
                @method('delete')
                <button type="submit" class="button is-danger" onclick="return confirm('Are you sure to delete question: ' + ' {{  str_limit($question->title, $limit = 20, $end = '...')}} ?' );"> Delete Question </button>
            </form>
        </div>
    @endcan
</div>
</div>

<hr>
<div class="panel">
    <button class="button marginbottom20px is-link add_answer_button" > Answer </button>
</div>
<div class="panel  add_answer {{ $errors->any() ? ' display-block' : '' }}">
    <form method="Post" action="/answers">
        @csrf
        <div>
            <label class="label" for="description"> Answer  </label>
            <div>
                <textarea class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name='description'> {{old('description')}} </textarea>
            </div>
        </div>
        <div>
            <input type="hidden" name='question_id' value="{{ $question->id }}">
        </div>
        <div class="margintop10px">
            <button type="submit" class="button marginbottom10px is-link">Submit your answer </button>
        </div>
    </form>
</div>
<!-- validation -->
@if ($errors->any())
<div class="notification is-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<!-- end validation -->
<!---------- Answer list ------------->

@if($question->answers->count())
<div class="panel">
    @foreach($question->answers->reverse() as $answer)
    <form method="Post" action="/answers/{{ $answer->id }}">
        <div>
            @csrf
            @method('patch')
        </div>
        <!-- for username and date -------->
        <div>
            <ul>
             <li><img src="/images/comment.jpg" width="30px" height="30px"> {{ $answer->user->name}}</li>
                <li>
                    @if( $answer->created_at != $answer->updated_at )
                        <div>
                            <p> Updated: {{ $answer->updated_at  }} </p>
                        </div>
                    @else
                        <div>
                            <p> Created: {{ $answer->created_at  }} </p>
                        </div>
                    @endif
                </li>
            </ul>
        </div>
        <!-- End for username and date -------->
        <div class="columns">
            <div class="column is-four-fifths">
                 {{ $answer->description }}
            </div>

            @can('update', $answer)
                <div class="column">
                    <a class="button is-small is-link" href="/answers/{{$answer->id}}/edit"> Edit </a>
                </div>
                @endcan
                @can('delete', $answer)
                <div class="column">
                    <a href="/answers/{{$answer->id}}/deleteanswer" class="button is-small is-danger" onclick="return confirm('Are you sure to delete answer' + ' {{ str_limit($answer->description, $limit = 20, $end = '...') }} ?' );">Delete</a>
                </div>
            @endcan
        </div>
    </form>

        <!--  start like here -->
        <div class="margintop20px">
            <form method="Post" action="/answerUserLikes" class="formlike">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                @csrf
                <div>
                    <input type="hidden" name='question_id' class="like_question_id" value="{{ $question->id }}">
                    <input type="hidden" name='answer_id' class="like_answer_id" value="{{ $answer->id }}">
                    <input type="hidden" name='user_id' class="like_user_id" value="{{ Auth::id() }}">
                </div>
                <div class="margintop20px">
                    <p class="likeTotal" id="{{$answer->id }}"> {{$answer->answer_user_likes->count()<1? '':$answer->answer_user_likes->count() }} </p> <p class="imgLike"><img src="/images/like.png" width="30px" height="30px" onclick="$(this).closest('form').submit();"> </p>
                </div>
            </form>
        </div>
        <!-- end like here-->

    <!-- Start Comment here -->
    <div class="panel margintop20px">
     <a href="#" class="add_comment_link"> {{$answer->comments->count()}}  {{$answer->comments->count() > 1 ? 'Comments' : 'Comment' }}</a>  <!-- for toggle comments list and add comment -->

    <!-- Comment list -->
         <div class="commentsPanel box">
           @if($answer->comments->count())
             @foreach($answer->comments as $comment)
                 <div>
                    <p class="commentimg"><img src="/images/comment.jpg" width="30px" height="30px">  {{ $comment->user->name  }}  {{ $comment->created_at  }} </p>
                 </div>

                 <div class="columns">
                     <div class="column is-four-fifths">
                         <p>{{ $comment->description }} </p>
                     </div>
                     @can('update', $comment)
                         <div class="column">
                             <a class="button is-small is-link" href="/comments/{{$comment->id}}/edit"> Edit </a>
                         </div>
                     @endcan
                     @can('delete', $comment)
                         <div class="column">
                             <a href="/comments/{{$comment->id}}/deletecomment" class="button is-small is-danger" onclick="return confirm('Are you sure to delete comment' + ' {{ str_limit($comment->description, $limit = 20, $end = '...') }} ?' );">Delete</a>
                         </div>
                     @endcan
                 </div>
                 <hr>
             @endforeach
           @endif
             <!--  comment input box-->
                 <div class="panel add_comment {{ $errors->any() ? ' display-block' : '' }}">
                     <form method="Post" action="/comments">
                     @csrf
                         <div>
                             <div>
                                 <textarea required class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name='description'  rows="2">{{old('description')}}  </textarea>
                             </div>
                         </div>
                         <div>
                             <input type="hidden" name='question_id' value="{{ $question->id }}">
                             <input type="hidden" name='answer_id' value="{{ $answer->id }}">
                         </div>
                         <div class="margintop10px">
                             <button type="submit" class="button is-link">Submit </button>
                         </div>
                     </form>
                 </div>
             <!-- end comment input-->

         </div>
        <!--  End comment list -->
    </div>
        <!-- end Comment here -->

    <hr>
    @endforeach
</div>
@endif
<!-----------End answer --------------------->
@endif
@endsection

