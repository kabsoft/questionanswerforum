<?php
use App\Http\Controllers\QuestionsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//App::bind('IQuestionRepo', 'QuestionDB');

/*
Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes(['verify' => true]);

Route::get('/home', 'QuestionsController@index')->name('Questions');
route::get('/', 'QuestionsController@index');

//questions
route::get('/questions', 'QuestionsController@index');
route::get('/questions/create', 'QuestionsController@create');
route::get('/questions/{question}', 'QuestionsController@show');
route::get('/questions/{question}/edit', 'QuestionsController@edit');
route::Post('/questions', 'QuestionsController@store');
route::patch('/questions/{question}', 'QuestionsController@update');
route::delete('/questions/{question}', 'QuestionsController@destroy');
//documentation
route::get('/documentation', 'QuestionsController@documentation');


//answers
route::get('/answers/{answer}', 'QuestionAnswersController@show');
route::get('/answers/{answer}/edit', 'QuestionAnswersController@edit');
route::get('/answers/{answer}/deleteanswer', 'QuestionAnswersController@deleteAnswer');
route::patch('/answers/{answer}/answerupdate', 'QuestionAnswersController@answerupdate');
route::post('/answers', 'QuestionAnswersController@store');
route::delete('/answers/{answer}', 'QuestionAnswersController@destroy');

//comments
route::get('/comments/{comment}', 'AnswerCommentsController@show');
route::get('/comments/{comment}/edit', 'AnswerCommentsController@edit');
route::get('/comments/{comment}/deletecomment', 'AnswerCommentsController@deleteComment');
route::patch('/comments/{comment}/commentupdate', 'AnswerCommentsController@commentupdate');
route::post('/comments', 'AnswerCommentsController@store');
route::delete('/comments/{comment}', 'AnswerCommentsController@destroy');


//likes
route::post('/answerUserLikes', 'AnswerUserLikesController@like');



